\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Motivation}{3}{0}{1}
\beamer@sectionintoc {2}{Defenitions}{15}{0}{2}
\beamer@sectionintoc {3}{Problems and Challenges}{18}{0}{3}
\beamer@sectionintoc {4}{Parallel Processing Patterns}{32}{0}{4}
\beamer@sectionintoc {5}{Parallel Programming Models}{37}{0}{5}
