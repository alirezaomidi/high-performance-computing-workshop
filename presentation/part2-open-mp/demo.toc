\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Case Study}{3}{0}{1}
\beamer@sectionintoc {2}{Shared Memory Architecture}{11}{0}{2}
\beamer@sectionintoc {3}{Programming Model}{14}{0}{3}
\beamer@sectionintoc {4}{Race Condition}{26}{0}{4}
