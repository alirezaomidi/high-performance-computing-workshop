#include <iostream>
#include <cmath>
#include <ctime>
#include <omp.h>

#define accu 100

using namespace std;

double integral(double (*func) (double) , double begin , double end){
    double result = 0;
    #pragma omp parallel for reduction (+:result)
    for (int i = 0 ; i < accu ; i++){
        double point = (double)i / accu;
        result += func(point) * (1.0 / accu);
    }

    return result;
}

double pi (double point){
    return((4.0)/(1.0 + pow(point , 2)));
}

int main (){

    clock_t start = clock();

    cout << integral(pi , 0 , 1) << endl;

    clock_t end = clock();

    double t = (double)(end - start)/(CLOCKS_PER_SEC);
    cout << "terminated in " << t << " seconds" << endl;

    return 0;
}
