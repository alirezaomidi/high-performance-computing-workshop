#include <iostream>
#include <cmath>
#include <ctime>

#define accu 100

using namespace std;

double integral(double (*func) (double) , double begin , double end){
    double result = 0;
    for (double point = begin; point <= end ; point += (1.0 / accu)){
        result += func(point) * (1.0 / accu);
    }

    return result;
}

double pi (double point){
    return((4.0)/(1.0 + pow(point , 2)));
}

int main (){

    clock_t start = clock();

    cout << integral(pi , 0 , 1) << endl;

    clock_t end = clock();

    double t = (double)(end - start)/(CLOCKS_PER_SEC);
    cout << "terminated in " << t << " seconds" << endl;

    return 0;
}
