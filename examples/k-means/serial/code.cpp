#include <iostream>
#include <climits>
#include <vector>
#include <cmath>
#include <omp.h>

using namespace std;

double dist (vector <double> a , vector <double> b){
    int result = 0;

    for (int i = 1 ; i < a.size() ; i++)
        result += pow((a[i] - b[i]) , 2);
    result = sqrt(result);
    return result;

    // EUCL distance
}

int main (){

    // Reading the file
    size_t no_instance = 434874;
    size_t no_attr = 4;
    size_t clusters = 5;

    // Reading Data File
    vector < vector <double> > dataset;
    for (int i = 0 ; i < no_instance ; i++){
        vector <double> record;
        for (int j = 0 ; j < no_attr; j++){
            double temp; cin >> temp; record.push_back(temp);
        } dataset.push_back(record);
    }

    cerr << "file loaded!" << endl;

    vector < vector <double> > centroids;

    vector <size_t> belongs_to (no_instance , -1);
    vector <size_t> cluster_size(clusters , 0);
    // initializing centroids
    for (int i = 0 ; i < clusters; i++){
        centroids.push_back(dataset[i]);
    }
    for (int iteration = 0 ; iteration < 100 ; iteration++){
        // cerr << "Entered Iter " << iteration << endl;
        // Phase 1: Assigning each node to a clusters
        #pragma omp parallel for
        for (int i = 0 ; i < no_instance ; i++){
            double min_dist = INT_MAX;
            int nearest = -1;
            for (int c = 0 ; c < clusters ; c++){
                int node_cent_dist = dist(centroids[c] , dataset[i]);
                if (min_dist > node_cent_dist){
                    nearest = c;
                    min_dist = node_cent_dist;
                }
            }
            belongs_to[i] = nearest;
        }


        // Phase 2: Repositioning Centroids
        for (int c = 0 ; c < clusters ; c++) {
            for (int attr = 1 ; attr < no_attr ; attr++)
                centroids[c][attr] = 0;
            cluster_size[c] = 0;
        }
        for (int i = 0 ; i < no_instance ; i++){
            for (int attr = 1 ; attr < no_attr ; attr++)
                centroids[belongs_to[i]][attr] += dataset[i][attr];
            cluster_size[belongs_to[i]] ++;
        }

        for (int c = 0 ; c < clusters ; c++) {
            // cerr << "Cluster " << c << "is : ";
            for (int attr = 1 ; attr < no_attr ; attr++){
                centroids[c][attr] /= (double)cluster_size[c];
                // cerr << centroids[c][attr] << " ";
             }
            // cerr << endl;
        }
    }


    return 0;
}
