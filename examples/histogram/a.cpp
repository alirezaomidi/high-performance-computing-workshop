#include <iostream>
#include <vector>
#include <ctime>
using namespace std;

#define datasize 10

vector<int> data;
vector <pair <int , int> > beans;
vector <int> beanscount;

int find_index(int datapoint , int start = 0 , int end = beans.size()){
    int index = (start + end) / 2;
    if (beans[index].first <= datapoint && datapoint < beans[index].second){
        return index;
    }
    else if (beans[index].first <= datapoint)
        return find_index(datapoint , index , end);
    else
        return find_index(datapoint , start , index);
}

int main (){

    beans.push_back(make_pair(0, 20)); beanscount.push_back(0);
    beans.push_back(make_pair(20, 40)); beanscount.push_back(0);
    beans.push_back(make_pair(40, 60)); beanscount.push_back(0);
    beans.push_back(make_pair(60, 80)); beanscount.push_back(0);
    beans.push_back(make_pair(80, 100)); beanscount.push_back(0);
    //
    // for (int i = 0 ; i < datasize ; i++){
    //     int temp; cin >> temp;
    //     data.push_back(temp);
    // }

    srand (time(0));
    for (int i = 0 ; i < datasize ; i++){
        int temp = rand() % 100;
        data.push_back(temp);
    } for (int i = 0 ; i < datasize ; i++) cout << data[i] << endl;

    for (int i = 0 ; i < datasize ; i++){
        beanscount[find_index(data[i])] ++;
    }


    for (int i = 0 ; i < beanscount.size() ; i++){
        cout << "bean # " << i + 1 << " includes : " << beanscount[i] << endl;
    }

    return 0;
}
