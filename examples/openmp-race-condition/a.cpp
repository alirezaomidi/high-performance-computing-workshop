#include <iostream>
#include <omp.h>

using namespace std;

int main (){
    int result = 0;
    #pragma omp parallel
    {
        for (int i = 0 ; i < 100 ; i++){
            #pragma omp atomic
            result += 1;
        }
    }
    cout << result << endl;

    return 0;
}
